### Summary ###
* Data Analysis for Credit Card Fraud 
* Python 3, using Jupyter Notebook

### How do I get set up? ###
* Dependencies


``` pip3 install numpy pandas sklearn imblearn seaborn```


* In your command line (make sure you're in the right directory && make sure both the csv file and the ipynb are in the same directory)


``` jupyter notebook credit_card_fraud.ipynb```

### How can I view without cloning the repo? ###
* Head to the ipynb file, and change the view from "Default File Viewer" to "IPython Notebook"